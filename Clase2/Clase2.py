import cv2 as cv

image = cv.imread('./imread/Logo.jpg')
#para cambiar a blanco y negro la imagen
imagebw = cv.cvtColor(image, cv.COLOR_BGR2GRAY)
image2 = cv.imwrite('./imwrite/Gray_Image.jpg', imagebw)

cv.imshow( 'Color_Image', image )
cv.imshow( 'Gray_image', image2 )
cv.waitKey(0);

#para cambiar a negativo la imagen
image = cv.imread('./imread/Logo.jpg')
imagebw = cv.cvtColor(image, cv.COLOR_BGR2GRAY)
image2 = cv.imwrite('./imwrite/Gray_Image.jpg', imagebw)

imageneg = 255 - image
image3 = cv.imwrite('./imwrite/Gray_Image.jpg', imageneg)
cv.imshow( 'Color_Image', image )
cv.imshow( 'Gray_image', image2 )
cv.imshow( 'Negative_image', image3 )
cv.waitKey(0);